import * as http from "http";

// Define a custom interface that extends the IncomingMessage interface to include the 'body' property
interface CustomIncomingMessage extends http.IncomingMessage {
  body: any;
}

export class Router {
  private routes: { [key: string]: Handler<any> } = {};

  get<T>(path: string, handler: Handler<T>): this {
    if (this.routes[`GET:${path}`]) {
      throw new Error(`Route GET:${path} already exists`);
    }

    this.routes[`GET:${path}`] = handler;
    return this;
  }

  post<T>(path: string, handler: Handler<T>): this {
    if (this.routes[`POST:${path}`]) {
      throw new Error(`Route POST:${path} already exists`);
    }

    this.routes[`POST:${path}`] = handler;
    return this;
  }

  put<T>(path: string, handler: Handler<T>): this {
    if (this.routes[`PUT:${path}`]) {
      throw new Error(`Route PUT:${path} already exists`);
    }

    this.routes[`PUT:${path}`] = handler;
    return this;
  }

  delete<T>(path: string, handler: Handler<T>): this {
    if (this.routes[`DELETE:${path}`]) {
      throw new Error(`Route DELETE:${path} already exists`);
    }

    this.routes[`DELETE:${path}`] = handler;
    return this;
  }

  getHandler() {
    return async (
      request: CustomIncomingMessage, // Use the custom interface here
      response: http.ServerResponse
    ) => {
      const method = request.method || "";
      const url = request.url || "";
      const handlerKey = `${method}:${url}`;

      const handler = this.routes[handlerKey];

      if (handler) {
        try {
          if (method === "POST" || method === "PUT") {
            // Parse request body for POST and PUT requests
            const requestBody = await this.parseRequestBody(request);
            request.body = requestBody; // Assign the parsed body to the 'body' property
          }
          await handler(request, response);
        } catch (error) {
          this.handleError(response, 500, "Internal Server Error");
        }
      } else {
        this.handleError(response, 404, "Not Found");
      }
    };
  }

  private async parseRequestBody(request: CustomIncomingMessage): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      let requestBody = "";

      request.on("data", (chunk) => {
        requestBody += chunk.toString();
      });

      request.on("end", () => {
        try {
          const parsedBody = JSON.parse(requestBody);
          resolve(parsedBody);
        } catch (error) {
          reject(error);
        }
      });

      request.on("error", (error) => {
        reject(error);
      });
    });
  }

  private handleError(
    response: http.ServerResponse,
    statusCode: number,
    errorMessage: string
  ) {
    console.error(errorMessage);
    response.setHeader("Content-Type", "text/plain");
    response.statusCode = statusCode;
    response.end(errorMessage);
  }
}

export type Handler<T> = (
  request: CustomIncomingMessage,
  response: http.ServerResponse
) => Promise<void>;

// Create a sample HTTP server using Node.js built-in 'http' module
const server = http.createServer();

// Create an instance of the Router class
const router = new Router();

// Define some sample route handlers
router.get("/hello", async (request, response) => {
  response.setHeader("Content-Type", "text/plain");
  response.statusCode = 200;
  response.end("Hello, World!");
});

router.post("/echo", async (request, response) => {
  const requestBody = request.body;
  response.setHeader("Content-Type", "application/json");
  response.statusCode = 200;
  response.end(JSON.stringify(requestBody));
});

// Set the request handler for the server
server.on("request", router.getHandler());

// Start the server and listen on port 3000
server.listen(3000, () => {
  console.log("Server is listening on port 3000");
});
